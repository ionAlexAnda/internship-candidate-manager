package pentastagiu.candidate_management.dto;

import java.util.Set;

import pentastagiu.candidate_management.model.InternshipOption;

public class CandidateDTO {
	private Long id;
	private String lastName;
	private String firstName;
	private String email;
	private String phoneNumber;
	private Long yearOfStudies;
	private String faculty;
	private Set<InternshipOption> optionList;
	private String linkToCV;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getYearOfStudies() {
		return yearOfStudies;
	}

	public void setYearOfStudies(Long yearOfStudies) {
		this.yearOfStudies = yearOfStudies;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public Set<InternshipOption> getOptionList() {
		return optionList;
	}

	public void setOptionList(Set<InternshipOption> optionList) {
		this.optionList = optionList;
	}

	public String getLinkToCV() {
		return linkToCV;
	}

	public void setLinkToCV(String linkToCV) {
		this.linkToCV = linkToCV;
	}

}
