package pentastagiu.candidate_management.model;


import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Candidate {
	@Id
	@GeneratedValue
	private Long id;

	private String lastName;
	private String firstName;

	@OneToOne
	private Contact contact;

	@OneToOne
	private Studies studies;

	@ManyToMany
	private Set<InternshipOption> optionList;

	private Status status;

	private String linkToCV;

	public enum Status {

		ACCEPTED, REJECTED, REGISTERED
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Studies getStudies() {
		return studies;
	}

	public void setStudies(Studies studies) {
		this.studies = studies;
	}

	public Set<InternshipOption> getOptionList() {
		return optionList;
	}

	public void setOptionList(Set<InternshipOption> optionList) {
		this.optionList = optionList;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getLinkToCV() {
		return linkToCV;
	}

	public void setLinkToCV(String linkToCV) {
		this.linkToCV = linkToCV;
	}
}
