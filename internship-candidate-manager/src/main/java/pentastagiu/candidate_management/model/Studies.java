package pentastagiu.candidate_management.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Studies {
	@Id
	@GeneratedValue
	private Long id;
	private Long yearOfStudies;
	private String faculty;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getYearOfStudies() {
		return yearOfStudies;
	}

	public void setYearOfStudies(Long yearOfStudies) {
		this.yearOfStudies = yearOfStudies;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
}
