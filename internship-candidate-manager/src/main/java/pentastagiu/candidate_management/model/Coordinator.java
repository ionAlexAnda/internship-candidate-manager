package pentastagiu.candidate_management.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Coordinator {
	@Id
	@GeneratedValue
	private Long id;

	private String lastName;
	private String firstName;

	@OneToOne
	private Contact contact;

	@OneToOne
	private InternshipOption intershipOption;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public InternshipOption getIntershipOption() {
		return intershipOption;
	}

	public void setIntershipOption(InternshipOption intershipOption) {
		this.intershipOption = intershipOption;
	}
}
