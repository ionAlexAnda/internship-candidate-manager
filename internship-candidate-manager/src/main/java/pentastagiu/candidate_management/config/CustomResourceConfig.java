package pentastagiu.candidate_management.config;

import org.glassfish.jersey.server.ResourceConfig;

public class CustomResourceConfig extends ResourceConfig {
	public CustomResourceConfig() {
		register(new CustomBinder());
		packages(true, "pentastagiu.candidate_management.ws");
	}

}
