package pentastagiu.candidate_management.config;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import pentastagiu.candidate_management.dao.CandidateDAO;
import pentastagiu.candidate_management.dao.ContactDAO;
import pentastagiu.candidate_management.dao.InternshipOptionDAO;
import pentastagiu.candidate_management.dao.StudiesDAO;
import pentastagiu.candidate_management.dao.config.EntityManagerConfigurator;
import pentastagiu.candidate_management.dao.impl.CandidateDAOImpl;
import pentastagiu.candidate_management.dao.impl.ContactDAOImpl;
import pentastagiu.candidate_management.dao.impl.InternshipOptionDAOImpl;
import pentastagiu.candidate_management.dao.impl.StudiesDAOImpl;
import pentastagiu.candidate_management.service.CandidateService;
import pentastagiu.candidate_management.service.ContactService;
import pentastagiu.candidate_management.service.InternshipOptionService;
import pentastagiu.candidate_management.service.StudiesService;
import pentastagiu.candidate_management.service.impl.CandidateServiceImpl;
import pentastagiu.candidate_management.service.impl.ContactServiceImpl;
import pentastagiu.candidate_management.service.impl.InternshipOptionServiceImpl;
import pentastagiu.candidate_management.service.impl.StudiesServiceImpl;

public class CustomBinder extends AbstractBinder {

	@Override
	protected void configure() {
		bind(CandidateServiceImpl.class).to(CandidateService.class);
		bind(CandidateDAOImpl.class).to(CandidateDAO.class);
		bind(ContactServiceImpl.class).to(ContactService.class);
		bind(ContactDAOImpl.class).to(ContactDAO.class);
		bind(StudiesServiceImpl.class).to(StudiesService.class);
		bind(StudiesDAOImpl.class).to(StudiesDAO.class);
		bind(InternshipOptionDAOImpl.class).to(InternshipOptionDAO.class);
		bind(InternshipOptionServiceImpl.class).to(InternshipOptionService.class);
		bind(EntityManagerConfigurator.class).to(EntityManagerConfigurator.class);
	}
}
