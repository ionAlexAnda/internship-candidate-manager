package pentastagiu.candidate_management.dao;

import java.util.List;

import pentastagiu.candidate_management.model.Contact;

public interface ContactDAO {
	Contact findContactByEmail(String email);

	List<Contact> findAllContacts();

	Contact insertContact(Contact contact);

	Long deleteContact(Long contactId);

	Contact updateContact(Contact contact);
}
