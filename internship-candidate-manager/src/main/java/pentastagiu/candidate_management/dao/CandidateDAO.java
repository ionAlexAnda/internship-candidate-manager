package pentastagiu.candidate_management.dao;

import java.util.List;

import pentastagiu.candidate_management.model.Candidate;

public interface CandidateDAO {

	List<Candidate> findAll();

	Candidate findCandidateById(Long candidateId);

	Candidate insertCandidate(Candidate candidate);

	Long deleteCandidate(Long candidateId);

	Candidate updateCandidate(Candidate candidate);
}
