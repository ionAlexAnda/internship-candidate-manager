package pentastagiu.candidate_management.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import pentastagiu.candidate_management.dao.InternshipOptionDAO;
import pentastagiu.candidate_management.dao.config.EntityManagerConfigurator;
import pentastagiu.candidate_management.model.InternshipOption;

public class InternshipOptionDAOImpl implements InternshipOptionDAO {
	@Inject
	private EntityManagerConfigurator entityManagerConfigurator;

	public List<InternshipOption> listAll() {
		Query query = entityManagerConfigurator.getEntityManager().createQuery("SELECT io FROM InternshipOption io");
		List<InternshipOption> internships = new ArrayList<InternshipOption>();

		try {
			entityManagerConfigurator.getEntityManager().getCriteriaBuilder();
			internships = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return internships;
	}

	public InternshipOption insertInternship(InternshipOption internship) {
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			entityManagerConfigurator.getEntityManager().persist(internship);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return internship;
	}

	public Long deleteInternship(Long internshipId) {
		InternshipOption internship = null;

		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			internship = entityManagerConfigurator.getEntityManager().find(InternshipOption.class, internshipId);
			entityManagerConfigurator.getEntityManager().remove(internship);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return internshipId;
	}

}
