package pentastagiu.candidate_management.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import pentastagiu.candidate_management.dao.CandidateDAO;
import pentastagiu.candidate_management.dao.config.EntityManagerConfigurator;
import pentastagiu.candidate_management.model.Candidate;

public class CandidateDAOImpl implements CandidateDAO {

	@Inject
	private EntityManagerConfigurator entityManagerConfigurator;

	public List<Candidate> findAll() {
		Query query = entityManagerConfigurator.getEntityManager().createQuery("SELECT c FROM Candidate c");
		List<Candidate> candidateList = new ArrayList<Candidate>();
		try {
			entityManagerConfigurator.getEntityManager().getCriteriaBuilder();
			candidateList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return candidateList;
	}

	public Candidate findCandidateById(Long candidateId) {
		Candidate candidate = null;
		try {
			candidate = entityManagerConfigurator.getEntityManager().find(Candidate.class, candidateId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidate;
	}

	public Candidate insertCandidate(Candidate candidate) {
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			entityManagerConfigurator.getEntityManager().persist(candidate);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidate;
	}

	public Long deleteCandidate(Long candidateId) {
		Candidate candidate = null;
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			candidate = entityManagerConfigurator.getEntityManager().find(Candidate.class, candidateId);
			entityManagerConfigurator.getEntityManager().remove(candidate);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidateId;
	}

	public Candidate updateCandidate(Candidate candidate) {
		Candidate candidateResponse = null;
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			candidateResponse = entityManagerConfigurator.getEntityManager().merge(candidate);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidateResponse;
	}
}
