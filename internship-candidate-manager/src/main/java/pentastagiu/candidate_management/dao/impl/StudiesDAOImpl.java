package pentastagiu.candidate_management.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import pentastagiu.candidate_management.dao.StudiesDAO;
import pentastagiu.candidate_management.dao.config.EntityManagerConfigurator;
import pentastagiu.candidate_management.model.Studies;

public class StudiesDAOImpl implements StudiesDAO {
	@Inject
	private EntityManagerConfigurator entityManagerConfigurator;

	public List<Studies> listAll() {
		List<Studies> studies = new ArrayList<Studies>();
		Query query = entityManagerConfigurator.getEntityManager().createQuery("SELECT s FROM Studies s");

		try {
			entityManagerConfigurator.getEntityManager().getCriteriaBuilder();
			studies = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return studies;
	}

	public Studies updateStudies(Studies studies) {
		Studies studiesResponse = null;
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			studiesResponse = entityManagerConfigurator.getEntityManager().merge(studies);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return studiesResponse;
	}

}
