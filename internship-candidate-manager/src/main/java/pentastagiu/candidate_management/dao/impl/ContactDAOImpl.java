package pentastagiu.candidate_management.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import pentastagiu.candidate_management.dao.ContactDAO;
import pentastagiu.candidate_management.dao.config.EntityManagerConfigurator;
import pentastagiu.candidate_management.model.Contact;

public class ContactDAOImpl implements ContactDAO {
	@Inject
	private EntityManagerConfigurator entityManagerConfigurator;

	public Contact findContactByEmail(String email) {
		Contact contacts = null;
		Query query = entityManagerConfigurator.getEntityManager()
				.createQuery("SELECT c FROM Contact c WHERE c.email LIKE :email");
		query.setParameter("email", email);
		try {
			entityManagerConfigurator.getEntityManager().getCriteriaBuilder();
			contacts = (Contact) query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contacts;
	}

	public List<Contact> findAllContacts() {
		Query query = entityManagerConfigurator.getEntityManager().createQuery("SELECT c FROM Contact c");
		List<Contact> contactList = new ArrayList<Contact>();
		try {
			entityManagerConfigurator.getEntityManager().getCriteriaBuilder();
			contactList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return contactList;
	}

	public Contact insertContact(Contact contact) {
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			entityManagerConfigurator.getEntityManager().persist(contact);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contact;
	}

	public Long deleteContact(Long contactId) {
		Contact contact = null;
		try {

			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			contact = entityManagerConfigurator.getEntityManager().find(Contact.class, contactId);
			entityManagerConfigurator.getEntityManager().remove(contact);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return contactId;
	}

	public Contact updateContact(Contact contact) {
		Contact contactResponse = null;
		try {
			entityManagerConfigurator.getEntityManager().getTransaction().begin();
			contactResponse = entityManagerConfigurator.getEntityManager().merge(contact);
			entityManagerConfigurator.getEntityManager().getTransaction().commit();
		} catch (Exception e) {

			e.printStackTrace();

		}
		return contactResponse;
	}
}
