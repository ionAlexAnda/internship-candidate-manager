package pentastagiu.candidate_management.dao;

import java.util.List;

import pentastagiu.candidate_management.model.Studies;

public interface StudiesDAO {
	List<Studies> listAll();

	Studies updateStudies(Studies studies);
}
