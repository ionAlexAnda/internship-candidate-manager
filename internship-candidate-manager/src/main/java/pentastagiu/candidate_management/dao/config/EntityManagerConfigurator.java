package pentastagiu.candidate_management.dao.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerConfigurator {

	private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("candidate_persistence");
	private EntityManager entityManager = entityManagerFactory.createEntityManager();

	public EntityManager getEntityManager() {
		return entityManager;
	}
}
