package pentastagiu.candidate_management.dao;

import java.util.List;

import pentastagiu.candidate_management.model.InternshipOption;

public interface InternshipOptionDAO {
	List<InternshipOption> listAll();

	InternshipOption insertInternship(InternshipOption intership);

	Long deleteInternship(Long internshipId);
}
