package pentastagiu.candidate_management.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.candidate_management.dto.CandidateDTO;
import pentastagiu.candidate_management.model.Candidate;
import pentastagiu.candidate_management.service.CandidateService;

@Path("/candidate")
public class CandidateWS {
	@Inject
	private CandidateService candidateService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Candidate> getAllCandidates() {
		return candidateService.findAll();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Candidate getCandidateById(@PathParam("id") Long candidateId) {
		return candidateService.findCandidateById(candidateId);
	}

	/**
	 * <pre>
	 {
	   "person":{ "firstName" : "Firicel" , "lastName" : "Celentano" },
	   "contacts":{ "email" : "vasilica@yahoo.com" , "phoneNumber" : "1456987" },
	   "studies":{ "yearOfStudies" : 3 , "faculty" : "AC"},
	   "optionList":[
	                   { "priority" : 1 , "name" : "Java"},
	                   { "priority" : 2 , "name" : "PHP"},
	                   { "priority" : 3 , "name" : ".Net"}
	               ],
	   "linkToCV":"http://safhdslsdhlgksglfs"
	}
	 * </pre>
	 * 
	 * @param candidateDTO
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Candidate insertCandidate(CandidateDTO candidateDTO) {
		return candidateService.insertCandidate(candidateDTO);
	}

	@DELETE
	@Path("{id}")
	public Long deleteCandidate(@PathParam("id") Long candidateId) {
		return candidateService.deleteCandidate(candidateId);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Candidate updateEmployee(Candidate candidate) {
		return candidateService.updateCandidate(candidate);
	}
}
