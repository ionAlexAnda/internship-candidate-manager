package pentastagiu.candidate_management.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.candidate_management.model.Contact;
import pentastagiu.candidate_management.service.ContactService;

@Path("/contact")
public class ContactWS {

	@Inject
	private ContactService contactService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> findAllContacts() {
		return contactService.findAllContacts();
	};

	@GET
	@Path("email")
	@Produces(MediaType.APPLICATION_JSON)
	public Contact findContactByEmail(@PathParam("email") String email) {
		return contactService.findContactByEmail(email);
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Long deleteContact(Long contactId) {
		return contactService.deleteContact(contactId);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Contact insertContact(Contact contact) {
		return contactService.insertCosntact(contact);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Contact updateContact(Contact contact) {
		return contactService.updateContact(contact);
	}
}
