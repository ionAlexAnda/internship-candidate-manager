package pentastagiu.candidate_management.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.candidate_management.model.Studies;
import pentastagiu.candidate_management.service.StudiesService;

@Path("/studies")
public class StudiesWS {
	@Inject
	private StudiesService studiesService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Studies> listAll() {
		return studiesService.listAll();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Studies updateStudies(Studies studies) {
		return studiesService.updateStudies(studies);
	}
}
