package pentastagiu.candidate_management.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.candidate_management.model.InternshipOption;
import pentastagiu.candidate_management.service.InternshipOptionService;

@Path("/internship")
public class InternshipOptionWS {
	@Inject
	private InternshipOptionService internshipOptionService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<InternshipOption> listAll() {
		return internshipOptionService.listAll();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public InternshipOption insertInternship(InternshipOption internship) {
		return internshipOptionService.insertInternship(internship);
	}

	@DELETE
	@Path("{id}")
	public Long deleteInternship(Long internshipId) {
		return internshipOptionService.deleteInternship(internshipId);
	}
}
