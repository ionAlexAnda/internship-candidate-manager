package pentastagiu.candidate_management.service;

import java.util.List;

import pentastagiu.candidate_management.dto.CandidateDTO;
import pentastagiu.candidate_management.model.Candidate;

public interface CandidateService {

	List<Candidate> findAll();

	Candidate findCandidateById(Long candidateId);

	Candidate insertCandidate(CandidateDTO candidateDTO);

	Long deleteCandidate(Long candidateId);

	Candidate updateCandidate(Candidate candidate);
}
