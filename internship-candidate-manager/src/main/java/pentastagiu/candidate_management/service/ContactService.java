package pentastagiu.candidate_management.service;

import java.util.List;

import pentastagiu.candidate_management.model.Contact;

public interface ContactService {
	List<Contact> findAllContacts();

	Contact findContactByEmail(String email);

	Contact insertCosntact(Contact contact);

	Long deleteContact(Long contactId);

	Contact updateContact(Contact contact);
}
