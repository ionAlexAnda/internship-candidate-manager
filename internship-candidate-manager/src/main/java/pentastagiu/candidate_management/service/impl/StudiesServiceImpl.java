package pentastagiu.candidate_management.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.candidate_management.dao.StudiesDAO;
import pentastagiu.candidate_management.model.Studies;
import pentastagiu.candidate_management.service.StudiesService;

public class StudiesServiceImpl implements StudiesService {
	@Inject
	private StudiesDAO studiesDAO;

	public List<Studies> listAll() {
		return studiesDAO.listAll();
	}

	public Studies updateStudies(Studies studies) {
		return studiesDAO.updateStudies(studies);
	}

}
