package pentastagiu.candidate_management.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.candidate_management.dao.InternshipOptionDAO;
import pentastagiu.candidate_management.model.InternshipOption;
import pentastagiu.candidate_management.service.InternshipOptionService;

public class InternshipOptionServiceImpl implements InternshipOptionService {
	@Inject
	private InternshipOptionDAO internshipOptionDAO;

	public List<InternshipOption> listAll() {
		return internshipOptionDAO.listAll();
	}

	public InternshipOption insertInternship(InternshipOption internshipOption) {
		return internshipOptionDAO.insertInternship(internshipOption);
	}

	public Long deleteInternship(Long internshipId) {
		return internshipOptionDAO.deleteInternship(internshipId);
	}
}
