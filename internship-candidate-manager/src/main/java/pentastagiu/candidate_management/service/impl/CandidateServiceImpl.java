package pentastagiu.candidate_management.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.candidate_management.dao.CandidateDAO;
import pentastagiu.candidate_management.dto.CandidateDTO;
import pentastagiu.candidate_management.model.Candidate;
import pentastagiu.candidate_management.model.Contact;
import pentastagiu.candidate_management.model.Studies;
import pentastagiu.candidate_management.model.Candidate.Status;
import pentastagiu.candidate_management.service.CandidateService;

public class CandidateServiceImpl implements CandidateService {

	@Inject
	private CandidateDAO candidateDAO;

	public List<Candidate> findAll() {
		return candidateDAO.findAll();
	}

	public Candidate findCandidateById(Long candidateId) {
		return candidateDAO.findCandidateById(candidateId);
	}

	public Candidate insertCandidate(CandidateDTO candidateDTO) {
		Candidate candidate = candidateDTOtoCandidate(candidateDTO);
		return candidateDAO.insertCandidate(candidate);
	}

	private Candidate candidateDTOtoCandidate(CandidateDTO candidateDTO) {
		Candidate candidate = new Candidate();
		candidate.setFirstName(candidateDTO.getFirstName());
		candidate.setLastName(candidateDTO.getLastName());

		Contact contact = new Contact();
		contact.setEmail(candidateDTO.getEmail());
		contact.setPhoneNumber(candidateDTO.getPhoneNumber());
		candidate.setContact(contact);

		Studies studies = new Studies();
		studies.setFaculty(candidateDTO.getFaculty());
		studies.setYearOfStudies(candidateDTO.getYearOfStudies());
		candidate.setStudies(studies);

		candidate.setOptionList(candidateDTO.getOptionList());

		candidate.setLinkToCV(candidateDTO.getLinkToCV());

		candidate.setStatus(Status.REGISTERED);

		return candidate;
	}

	public Long deleteCandidate(Long candidateId) {
		return candidateDAO.deleteCandidate(candidateId);
	}

	public Candidate updateCandidate(Candidate candidate) {
		return candidateDAO.updateCandidate(candidate);
	}

}
