package pentastagiu.candidate_management.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.candidate_management.dao.ContactDAO;
import pentastagiu.candidate_management.model.Contact;
import pentastagiu.candidate_management.service.ContactService;

public class ContactServiceImpl implements ContactService {
	@Inject
	private ContactDAO contactDAO;

	public Contact findContactByEmail(String email) {
		return contactDAO.findContactByEmail(email);
	}

	public Contact insertCosntact(Contact contact) {
		return contactDAO.insertContact(contact);
	}

	public Long deleteContact(Long contactId) {
		return contactDAO.deleteContact(contactId);
	}

	public Contact updateContact(Contact contact) {
		return contactDAO.updateContact(contact);
	}

	public List<Contact> findAllContacts() {
		return contactDAO.findAllContacts();
	}

}
