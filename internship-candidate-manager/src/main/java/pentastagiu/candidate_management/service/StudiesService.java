package pentastagiu.candidate_management.service;

import java.util.List;

import pentastagiu.candidate_management.model.Studies;

public interface StudiesService {
	List<Studies> listAll();

	Studies updateStudies(Studies studies);
}
