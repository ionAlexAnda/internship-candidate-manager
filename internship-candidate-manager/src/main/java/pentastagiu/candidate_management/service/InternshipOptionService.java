package pentastagiu.candidate_management.service;

import java.util.List;

import pentastagiu.candidate_management.model.InternshipOption;

public interface InternshipOptionService {
	List<InternshipOption> listAll();

	InternshipOption insertInternship(InternshipOption intership);

	Long deleteInternship(Long internshipId);
}
