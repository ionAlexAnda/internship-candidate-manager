import { Component } from 'angular2/core';
import { HTTP_PROVIDERS } from 'angular2/http';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import 'rxjs/Rx'; // load the full rxjs

import { CoordinatorsComponent } from './coordinators/coordinators.component';
import { CoordinatorService } from './coordinators/coordinator.service';
import { CandidatesComponent } from './candidates/candidates.component';
import { CandidateService } from './candidates/candidate.service';
import { CONFIG } from './config';

@Component({
  selector: 'story-app',
  templateUrl: 'app/app.component.html',
  styles: [`
    nav ul {list-style-type: none;}
    nav ul li {padding: 4px;cursor: pointer;display:inline-block}
  `],
  directives: [ROUTER_DIRECTIVES],
  providers: [
    HTTP_PROVIDERS,
    ROUTER_PROVIDERS,
    CoordinatorService
  ]
})
@RouteConfig([
  { path: '/coordinators/...', name: 'Coordinators', component: CoordinatorsComponent},
  { path: '/candidates/...', name: 'Candidates', component: CandidatesComponent, useAsDefault: true }
])
export class AppComponent {
   pageTitle: string = 'Internship Candidates Manager';
 }
