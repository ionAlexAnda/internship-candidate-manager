import { Component, OnInit } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';

import { Candidate, CandidateService } from './candidate.service';

@Component({
  selector: 'story-candidates',
  templateUrl: './app/candidates/candidate-list.component.html',
  styles: [`
    .candidates {list-style-type: none;}
    *.candidates li {padding: 4px;cursor: pointer;}
  `],
  directives: [ROUTER_DIRECTIVES]
})
export class CandidateListComponent implements OnInit {
  candidates: Candidate[];

  constructor(private _candidateService: CandidateService) { }

  ngOnInit() {
    this.candidates = [];
    this._candidateService.getCandidates()
      .subscribe(candidates => this.candidates = candidates);
  }
}
