import { Injectable } from 'angular2/core';
import { Http, Response } from 'angular2/http';
import {Headers, RequestOptions} from 'angular2/http';

import { CONFIG } from '../config';

let candidatesUrl = CONFIG.baseUrls.candidates;

export interface Candidate {}

@Injectable()
export class CandidateService {
  constructor(private _http: Http) { }

  getCandidate(id: number) {
    return this.getCandidates()
      .map(candidates => candidates.find(candidate => candidate.id == id));
  }
  
  addCandidate(candidate: Candidate) {
    let body = JSON.stringify(candidate);
    let headers = new Headers({ 'Content-Type': 'application/json' });
	let options = new RequestOptions({ headers: headers });
    return this._http
      .post(`${candidatesUrl}`, body, options)
      .map(res => res.json().data)
      .subscribe(candidate => this.candidate = candidate);
  }

  deleteCandidate(candidate: Candidate) {
    return this._http
      .delete(`${candidatesUrl}/${candidate.id}`)
      .map(res => res.json())
      .subscribe(candidates => this.candidates = candidates);
  }

  getCandidates() {
    return this._http.get(candidatesUrl)
      .map((response: Response) => <Candidate[]>response.json())
  }

  updateCandidate(candidate: Candidate) {
    let body = JSON.stringify(candidate);
    let headers = new Headers({ 'Content-Type': 'application/json' });
	let options = new RequestOptions({ headers: headers });
    return this._http
      .put(`${candidatesUrl}`, body, options)
      .map(res => res.json().data)
      .subscribe(candidate => this.candidate = candidate);
  }
}