import { Component, Input, OnInit } from 'angular2/core';
import { RouteParams, Router, ROUTER_DIRECTIVES } from 'angular2/router';

import { Candidate, CandidateService } from './candidate.service';
import { EntityService } from '../entity.service';

@Component({
  selector: 'story-candidate',
  templateUrl: 'app/candidates/candidate.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [EntityService]
})
export class CandidateComponent implements OnInit {

  @Input() candidate: Candidate;

  editCandidate: Candidate = <Candidate>{};

  constructor(
    private _routeParams: RouteParams,
    private _router: Router,
    private _entityService: EntityService,
    private _candidateService: CandidateService) { }

  ngOnInit() {
    this._getCandidate();
  }

  private _gotoCandidates() {
    let route = ['Candidates', { id: this.candidate ? this.candidate.id : null }]
    this._router.navigate(route);
  }
  isAddMode() {
    let id = +this._routeParams.get('id');
    return isNaN(id);
  }

  private _getCandidate() {
    let id = +this._routeParams.get('id');
    this._candidateService.getCandidate(id)
      .subscribe((candidate: Candidate) => this._setEditCandidate(candidate));
  }

  private _setEditCandidate(candidate: Candidate) {
    if (candidate) {
      this.candidate = candidate;
      this.editCandidate = this._entityService.clone(this.candidate);
    } else {
      this._gotoCandidates();
    }
  }
  delete() {
   	this._candidateService.deleteCandidate(this.candidate);
  }

  save() {
    let candidate = this.candidate = this._entityService.merge(this.candidate, this.editCandidate);
    this._candidateService.updateCandidate(this.candidate);
  }
}
