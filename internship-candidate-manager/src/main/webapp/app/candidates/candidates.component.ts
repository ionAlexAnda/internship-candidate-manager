import { Component, OnInit } from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';

import { CandidateListComponent } from './candidate-list.component';
import { CandidateComponent } from './candidate.component';
import { CandidateService } from './candidate.service';

@Component({
  selector: 'story-candidates-root',
  template: `
    <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES],
  providers: [CandidateService]
})
@RouteConfig([
  { path: '/', name: 'Candidates', component: CandidateListComponent, useAsDefault: true },
	{ path: '../candidate/:id', name: 'Candidate', component: CandidateComponent }
])
export class CandidatesComponent { }
