import { Component, OnInit } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';
import { Observable } from 'rxjs/Rx';

import { Coordinator, CoordinatorService } from './coordinator.service';

@Component({
  selector: 'story-coordinators',
  templateUrl: './app/coordinators/coordinator-list.component.html',
  styles: [`
    .coordinators {list-style-type: none;}
    *.coordinators li {padding: 4px;cursor: pointer;}
  `],
  directives: [ROUTER_DIRECTIVES]
})
export class CoordinatorListComponent implements OnInit {
  coordinators: Observable<Coordinator[]>;

  constructor(private _coordinatorService: CoordinatorService) { }

  ngOnInit() {
    this.coordinators = this._coordinatorService.getCoordinators();
  }
}
