import { Component } from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';

import { CoordinatorComponent } from './coordinator.component';
import { CoordinatorListComponent } from './coordinator-list.component';
import { CoordinatorService } from './coordinator.service';

@Component({
  selector: 'story-coordinators-root',
  template: `
    <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES]
})
@RouteConfig([
  { path: '/', name: 'Coordinators', component: CoordinatorListComponent, useAsDefault: true },
	{ path: '../coordinator/:id', name: 'Coordinator', component: CoordinatorComponent }
])
export class CoordinatorsComponent { }
