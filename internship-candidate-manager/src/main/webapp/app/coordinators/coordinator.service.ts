import { Injectable } from 'angular2/core';
import { Http, Response } from 'angular2/http';

import { CONFIG } from '../config';

let coordinatorsUrl = CONFIG.baseUrls.coordinators;

export interface Coordinator {}

@Injectable()
export class CoordinatorService {
  constructor(private _http: Http) { }

  getCoordinators() {
    return this._http.get(coordinatorsUrl)
      .map((response: Response) => <Coordinator[]>response.json().data);
  }

  getCoordinator(id: number) {
    return this.getCoordinators()
      .map(coordinators => coordinators.find(coordinator => coordinator.id == id));
  }
}
