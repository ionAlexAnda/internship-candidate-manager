import { Component, Input, OnInit } from 'angular2/core';
import { RouteParams, Router, ROUTER_DIRECTIVES } from 'angular2/router';

import { Coordinator, CoordinatorService } from './coordinator.service';

@Component({
  selector: 'story-coordinator',
  templateUrl: 'app/coordinators/coordinator.component.html',
  directives: [ROUTER_DIRECTIVES]
})
export class CoordinatorComponent implements OnInit {
  @Input() coordinator: Coordinator;

  constructor(
    private _coordinatorService: CoordinatorService,
    private _routeParams: RouteParams,
    private _router: Router) { }

  ngOnInit() {
    if (!this.coordinator) {
      let id = +this._routeParams.get('id');
      this._coordinatorService.getCoordinator(id)
        .subscribe(coordinator => this._setEditCoordinator(coordinator));
    }
  }

  private _gotoCoordinators() {
    let route = ['Coordinators', { id: this.coordinator ? this.coordinator.id : null }]
    this._router.navigate(route);
  }

  private _setEditCoordinator(coordinator: Coordinator) {
    if (coordinator) {
      this.coordinator = coordinator;
    } else {
      this._gotoCoordinators();
    }
  }
}
