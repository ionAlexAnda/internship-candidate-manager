# Internship candidate manager #
## About this project ##

Internship candidate manager is an application meant to help the training responsibles gestionate the candidates that have registered to the Pentastagiu, capable to show, delete or update the informations about them.

## Configuration details ##
In order to run the application you need to install MySQL.
Create a new schema named "candidates_db".
In internship-candidate-manager/src/main/resources/META-INF/persistence.xml change the values for username and password according to your database.

## Reference Manual ##

The application allows you to perform the following requests:

### Add candidate ###

Method: POST

URL: <host>[:port]/internship-candidate-manager/rest/candidate

Input type: JSON

Example of input:
{

"person":{ "firstName" : "Andrei" , "lastName" : "Popescu" },

"contacts":{ "email" : "vasilica@yahoo.com" , "phoneNumber" : "1456987" },

"studies":{ "yearOfStudies" : 3 , "faculty" : "AC"},

"optionList":[

{ "priority" : 1 , "name" : "Java"},
{ "priority" : 2 , "name" : "PHP"},
{ "priority" : 3 , "name" : ".Net"}

],

"linkToCV":"http://safhdslsdhlgksglfs"
}

### Find candidate by id ###

Method: GET

URL: <host>[:port]/internship-candidate-manager/rest/candidate/<candidate.id>

Output type: JSON

Get all employees

### List all the candidates ###

Method: GET

URL: <host>[:port]/internship-candidate-manager/rest/candidate

Output type: JSON

### Update candidates ###

Method: PUT

URL: <host>[:port]/internship-candidate-manager/rest/candidate

Output type: JSON

Example of input:

{

"person":{ "firstName" : "Andrei" , "lastName" : "Tudor" },

"contacts":{ "email" : "vasilica@yahoo.com" , "phoneNumber" : "1456987" },

"studies":{ "yearOfStudies" : 4 , "faculty" : "AC"},

"optionList":[

{ "priority" : 1 , "name" : "Java"},
{ "priority" : 2 , "name" : "PHP"},
{ "priority" : 3 , "name" : ".Net"}
],

"linkToCV":"http://link.com/cv7123621"
}

### Delete employee ###

Method: DELETE

URL: <host>[:port]/internship-candidate-manager/rest/candidate/<candidate.id>